module matrix_data_primitives_test_helper_module
    use, intrinsic :: iso_fortran_env, only : &
            real32, &
            real64

    use :: util_api, only : &
            assert, &
            string, &
            dictionary, &
            tile

    use :: data_api, only : &
            storage_helper, &
            stream, &
            scratch_buffer, &
            get_scratch_buffer, &
            dependency_manager

    use :: tensor_api, only : &
            scalar, &
            matrix, &
            tensor, &
            copy_tensor, &
            create_tensor, &
            matrix_data_primitives, &
            matrix_data_primitives_factory

    implicit none
    private

    public :: matrix_data_primitives_test_helper

    type :: matrix_data_primitives_test_helper
    contains
        procedure :: run => run
        procedure, nopass :: run_real32 => run_real32
        procedure, nopass :: run_real64 => run_real64
        procedure, nopass :: run_complex64 => run_complex64
        procedure, nopass :: run_complex128 => run_complex128
    end type matrix_data_primitives_test_helper

    real(real32), parameter :: single = 1.0e-7
    real(real64), parameter :: double = 1.0d-15
contains
    subroutine run(this, assertion, prefix, driver, memory_type, options, priorities)
        class(matrix_data_primitives_test_helper), intent(in) :: this
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        call this%run_real32(assertion, prefix//"real32:", driver, memory_type, options, priorities)
        call this%run_real64(assertion, prefix//"real64:", driver, memory_type, options, priorities)
        call this%run_complex64(assertion, prefix//"complex64:", driver, memory_type, options, priorities)
        call this%run_complex128(assertion, prefix//"complex128:", driver, memory_type, options, priorities)
    end subroutine run

    subroutine run_real32(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        real(real32), dimension(11,13) :: original, regression
        real(real32), dimension(5,4) :: patch_regression
        type(matrix) :: src, dst, patch
        type(matrix_data_primitives) :: mdp
        type(stream) :: astream
        type(storage_helper) :: helper
        real(real32) :: tolerance
        type(scratch_buffer) :: scratch

        tolerance = single

        astream = dependency_manager%get_new_stream()
        call matrix_data_primitives_factory%create(mdp, driver, options, priorities)

        call random_number(original)
        patch_regression = original(2:6,4:7)

        call copy_tensor(src, original, memory_type, options, priorities)
        call mdp%allocate_and_copy_tile(dst, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Allocate and copy", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call dst%cleanup()

        call create_tensor(dst, [5,4], "real32", memory_type, options, priorities)
        call mdp%copy_tile(dst, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Copy", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call dst%cleanup()

        regression = original
        regression(2:6,4:7) = regression(2:6,4:7) + patch_regression
        call copy_tensor(patch, patch_regression, memory_type, options, priorities)
        call mdp%update_tile(src, tile(2,6,4,7), patch)
        call assertion%equal(prefix//"::Update", &
                helper%equal(src%storage, regression, tolerance))
        call patch%cleanup()

        call mdp%cleanup()
        call src%cleanup()

        ! Scratch buffer tests
        scratch = get_scratch_buffer(memory_type, options, priorities)
        call scratch%initialize()

        call matrix_data_primitives_factory%create(mdp, driver, options, priorities)

        call random_number(original)
        patch_regression = original(2:6,4:7)

        call copy_tensor(src, original, memory_type, options, priorities)
        call mdp%copy_tile_to_scratch(dst, scratch, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Copy tile to scratch", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call scratch%destroy(dst%storage)
        call dst%release()

        regression = original
        regression(2:6,4:7) = regression(2:6,4:7) + patch_regression
        call copy_tensor(patch, patch_regression, memory_type, options, priorities)
        call mdp%update_tile_using_scratch(src, scratch, tile(2,6,4,7), patch)
        call assertion%equal(prefix//"::Update using scratch", &
                helper%equal(src%storage, regression, tolerance))
        call patch%cleanup()
        call scratch%checkpoint()

        call dependency_manager%destroy_stream(astream)
        call mdp%cleanup()
        call src%cleanup()
        call scratch%cleanup()
    end subroutine run_real32

    subroutine run_real64(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        real(real64), dimension(11,13) :: original, regression
        real(real64), dimension(5,4) :: patch_regression
        type(matrix) :: src, dst, patch
        type(matrix_data_primitives) :: mdp
        type(stream) :: astream
        type(storage_helper) :: helper
        real(real64) :: tolerance
        type(scratch_buffer) :: scratch

        tolerance = double

        astream = dependency_manager%get_new_stream()
        call matrix_data_primitives_factory%create(mdp, driver, options, priorities)

        call random_number(original)
        patch_regression = original(2:6,4:7)

        call copy_tensor(src, original, memory_type, options, priorities)
        call mdp%allocate_and_copy_tile(dst, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Allocate and copy", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call dst%cleanup()

        call create_tensor(dst, [5,4], "real64", memory_type, options, priorities)
        call mdp%copy_tile(dst, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Copy", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call dst%cleanup()

        regression = original
        regression(2:6,4:7) = regression(2:6,4:7) + patch_regression
        call copy_tensor(patch, patch_regression, memory_type, options, priorities)
        call mdp%update_tile(src, tile(2,6,4,7), patch)
        call assertion%equal(prefix//"::Update", &
                helper%equal(src%storage, regression, tolerance))
        call patch%cleanup()

        call mdp%cleanup()
        call src%cleanup()

        ! Scratch buffer tests
        scratch = get_scratch_buffer(memory_type, options, priorities)
        call scratch%initialize()

        call matrix_data_primitives_factory%create(mdp, driver, options, priorities)

        call random_number(original)
        patch_regression = original(2:6,4:7)

        call copy_tensor(src, original, memory_type, options, priorities)
        call mdp%copy_tile_to_scratch(dst, scratch, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Copy tile to scratch", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call scratch%destroy(dst%storage)
        call dst%release()

        regression = original
        regression(2:6,4:7) = regression(2:6,4:7) + patch_regression
        call copy_tensor(patch, patch_regression, memory_type, options, priorities)
        call mdp%update_tile_using_scratch(src, scratch, tile(2,6,4,7), patch)
        call assertion%equal(prefix//"::Update using scratch", &
                helper%equal(src%storage, regression, tolerance))
        call patch%cleanup()
        call scratch%checkpoint()

        call dependency_manager%destroy_stream(astream)
        call mdp%cleanup()
        call src%cleanup()
        call scratch%cleanup()
    end subroutine run_real64

    subroutine run_complex64(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        complex(real32), dimension(11,13) :: original, regression
        complex(real32), dimension(5,4) :: patch_regression
        real(real32), dimension(11,13) :: r, i
        type(matrix) :: src, dst, patch
        type(matrix_data_primitives) :: mdp
        type(stream) :: astream
        type(storage_helper) :: helper
        complex(real32) :: tolerance
        type(scratch_buffer) :: scratch

        tolerance = (single,single)

        astream = dependency_manager%get_new_stream()
        call matrix_data_primitives_factory%create(mdp, driver, options, priorities)

        call random_number(r)
        call random_number(i)
        original = cmplx(r, i)
        patch_regression = original(2:6,4:7)

        call copy_tensor(src, original, memory_type, options, priorities)
        call mdp%allocate_and_copy_tile(dst, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Allocate and copy", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call dst%cleanup()

        call create_tensor(dst, [5,4], "complex64", memory_type, options, priorities)
        call mdp%copy_tile(dst, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Copy", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call dst%cleanup()

        regression = original
        regression(2:6,4:7) = regression(2:6,4:7) + patch_regression
        call copy_tensor(patch, patch_regression, memory_type, options, priorities)
        call mdp%update_tile(src, tile(2,6,4,7), patch)
        call assertion%equal(prefix//"::Update", &
                helper%equal(src%storage, regression, tolerance))
        call patch%cleanup()

        call mdp%cleanup()
        call src%cleanup()

        ! Scratch buffer tests
        scratch = get_scratch_buffer(memory_type, options, priorities)
        call scratch%initialize()

        call matrix_data_primitives_factory%create(mdp, driver, options, priorities)

        call random_number(r)
        call random_number(i)
        original = cmplx(r, i)
        patch_regression = original(2:6,4:7)

        call copy_tensor(src, original, memory_type, options, priorities)
        call mdp%copy_tile_to_scratch(dst, scratch, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Copy tile to scratch", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call scratch%destroy(dst%storage)
        call dst%release()

        regression = original
        regression(2:6,4:7) = regression(2:6,4:7) + patch_regression
        call copy_tensor(patch, patch_regression, memory_type, options, priorities)
        call mdp%update_tile_using_scratch(src, scratch, tile(2,6,4,7), patch)
        call assertion%equal(prefix//"::Update using scratch", &
                helper%equal(src%storage, regression, tolerance))
        call patch%cleanup()
        call scratch%checkpoint()

        call dependency_manager%destroy_stream(astream)
        call mdp%cleanup()
        call src%cleanup()
        call scratch%cleanup()
    end subroutine run_complex64

    subroutine run_complex128(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        complex(real64), dimension(11,13) :: original, regression
        complex(real64), dimension(5,4) :: patch_regression
        real(real64), dimension(11,13) :: r, i
        type(matrix) :: src, dst, patch
        type(matrix_data_primitives) :: mdp
        type(stream) :: astream
        type(storage_helper) :: helper
        complex(real64) :: tolerance
        type(scratch_buffer) :: scratch

        tolerance = (double,double)

        astream = dependency_manager%get_new_stream()
        call matrix_data_primitives_factory%create(mdp, driver, options, priorities)

        call random_number(r)
        call random_number(i)
        original = cmplx(r, i, kind=real64)
        patch_regression = original(2:6,4:7)

        call copy_tensor(src, original, memory_type, options, priorities)
        call mdp%allocate_and_copy_tile(dst, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Allocate and copy", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call dst%cleanup()

        call create_tensor(dst, [5,4], "complex128", memory_type, options, priorities)
        call mdp%copy_tile(dst, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Copy", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call dst%cleanup()

        regression = original
        regression(2:6,4:7) = regression(2:6,4:7) + patch_regression
        call copy_tensor(patch, patch_regression, memory_type, options, priorities)
        call mdp%update_tile(src, tile(2,6,4,7), patch)
        call assertion%equal(prefix//"::Update", &
                helper%equal(src%storage, regression, tolerance))
        call patch%cleanup()

        call mdp%cleanup()
        call src%cleanup()

        ! Scratch buffer tests
        scratch = get_scratch_buffer(memory_type, options, priorities)
        call scratch%initialize()

        call matrix_data_primitives_factory%create(mdp, driver, options, priorities)

        call random_number(r)
        call random_number(i)
        original = cmplx(r, i, kind=real64)
        patch_regression = original(2:6,4:7)

        call copy_tensor(src, original, memory_type, options, priorities)
        call mdp%copy_tile_to_scratch(dst, scratch, src, tile(2,6,4,7))
        call assertion%equal(prefix//"::Copy tile to scratch", &
                helper%equal(dst%storage, patch_regression, tolerance))
        call scratch%destroy(dst%storage)
        call dst%release()

        regression = original
        regression(2:6,4:7) = regression(2:6,4:7) + patch_regression
        call copy_tensor(patch, patch_regression, memory_type, options, priorities)
        call mdp%update_tile_using_scratch(src, scratch, tile(2,6,4,7), patch)
        call assertion%equal(prefix//"::Update using scratch", &
                helper%equal(src%storage, regression, tolerance))
        call patch%cleanup()
        call scratch%checkpoint()

        call dependency_manager%destroy_stream(astream)
        call mdp%cleanup()
        call src%cleanup()
        call scratch%cleanup()
    end subroutine run_complex128
end module matrix_data_primitives_test_helper_module
