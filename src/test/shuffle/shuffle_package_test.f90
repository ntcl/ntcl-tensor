! Auto-generated -- DO NOT MODIFY
module shuffle_package_test_module
    use :: util_api, only : &
            selector, &
            assert

    use :: shuffle_test_module, only : shuffle_test

    implicit none
    private

    public :: shuffle_package_test

    type :: shuffle_package_test
        type(selector) :: test_selector
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type shuffle_package_test

    interface shuffle_package_test
        module procedure constructor
    end interface shuffle_package_test

contains
    function constructor(aselector) result(this)
        type(selector), intent(in) :: aselector
        type(shuffle_package_test) :: this

        call this%clear()

        this%test_selector = aselector
    end function constructor

    subroutine run(this, assertion)
        class(shuffle_package_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(shuffle_test) :: ashuffle_test

        call assertion%equal("shuffle::Package test complete", .true.)

        if ( &
                this%test_selector%is_enabled("shuffle") ) then
            ashuffle_test = shuffle_test()
            call ashuffle_test%run(assertion)
            call ashuffle_test%cleanup()
        end if

    end subroutine run

    subroutine cleanup(this)
        class(shuffle_package_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(shuffle_package_test), intent(inout) :: this
    end subroutine clear
end module shuffle_package_test_module
