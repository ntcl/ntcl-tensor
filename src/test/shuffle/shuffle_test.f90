module shuffle_test_module
    use, intrinsic :: iso_fortran_env, only : &
            real64

    use :: util_api, only : &
            assert, &
            string, &
            dictionary, &
            slice

    use :: data_api, only : &
            memory_factory, &
            storage_helper

    use :: tensor_api, only : &
            shuffle_factory, &
            shuffle, &
            tensor, &
            allocate_and_copy_tensor

    implicit none
    private

    public :: shuffle_test

    type :: shuffle_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type shuffle_test

    interface shuffle_test
        module procedure constructor
    end interface shuffle_test
contains
    function constructor() result(this)
        type(shuffle_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(shuffle_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(string), dimension(:), allocatable :: drivers, memory_types
        integer :: idx, midx
        type(string) :: prefix, my_prefix
        type(dictionary) :: options
        type(string), dimension(1) :: priorities

        call assertion%equal("shuffle::Test complete", .true.)

        drivers = shuffle_factory%get_available_drivers()
        memory_types = memory_factory%get_available_memory_types()

        prefix = "shuffle::"
        call run_test(assertion, prefix%char_array//"default:")
        do idx = 1, size(drivers)
            do midx = 1, size(memory_types)
                my_prefix = prefix%char_array//"memory_type="//memory_types(midx)%char_array//","// &
                        "shuffle_driver="//drivers(idx)%char_array//":"
                call run_test(assertion, my_prefix%char_array, &
                        drivers(idx)%char_array, memory_types(midx)%char_array)
            end do
        end do

        options = dictionary()
        do idx = 1, size(drivers)
            do midx = 1, size(memory_types)
                call options%set_value("memory_type", memory_types(midx)%char_array)
                call options%set_value("shuffle_driver", drivers(idx)%char_array)
                my_prefix = prefix%char_array//"options(memory_type)="//memory_types(midx)%char_array//","// &
                        "options(shuffle_driver)="//drivers(idx)%char_array//":"
                call run_test(assertion, my_prefix%char_array, options=options)
            end do
        end do

        priorities(1) = "unittest-"
        do idx = 1, size(drivers)
            do midx = 1, size(memory_types)
                call options%set_value("memory_type", "dummy")
                call options%set_value("shuffle_driver", "dummy")
                call options%set_value("unittest-memory_type", memory_types(midx)%char_array)
                call options%set_value("unittest-shuffle_driver", drivers(idx)%char_array)
                my_prefix = prefix%char_array//"options(unittest-memory_type)="//memory_types(midx)%char_array//","// &
                        "options(unittest-shuffle_driver)="//drivers(idx)%char_array//":"
                call run_test(assertion, my_prefix%char_array, options=options, priorities=priorities)
            end do
        end do
    end subroutine run

    subroutine run_test(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        class(tensor), allocatable :: src, dst
        real(real64), dimension(10) :: org, vrf
        class(shuffle), allocatable :: ashuffle
        type(storage_helper) :: helper

        call shuffle_factory%create(ashuffle, driver, options, priorities)

        call random_number(org)
        vrf = 0.0d0

        call allocate_and_copy_tensor(src, org, memory_type, options, priorities)
        call allocate_and_copy_tensor(dst, vrf, memory_type, options, priorities)

        vrf = org
        call ashuffle%execute(dst, src)

        call assertion%equal(prefix//"::Straight copy", &
                helper%equal(dst%storage, vrf) )

        vrf(2:5) = org(6:9)
        call ashuffle%execute(dst, src, slice(2,5), slice(6,9))

        call assertion%equal(prefix//"::Sliced copy", &
                helper%equal(dst%storage, vrf) )

        vrf([2,6,8]) = org([7,4,3])
        call ashuffle%execute(dst, src, [2,6,8], [7,4,3])

        call assertion%equal(prefix//"::Random copy", &
                helper%equal(dst%storage, vrf) )
    end subroutine run_test

    subroutine cleanup(this)
        class(shuffle_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(shuffle_test), intent(inout) :: this
    end subroutine clear
end module shuffle_test_module
