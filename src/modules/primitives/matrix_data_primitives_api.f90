module matrix_data_primitives_api
    use :: util_api, only : &
            dictionary

    use :: matrix_data_primitives_module, only : matrix_data_primitives
    use :: mdp_factory_module, only : mdp_factory

    use :: matrix_data_primitives_interface, only : &
            add_tiled_matrix_data

    implicit none
    private

    public :: matrix_data_primitives
    public :: mdp_factory
    public :: matrix_data_primitives_factory
    public :: add_tiled_matrix_data
    public :: mdp_initialize
    public :: mdp_finalize

    class(mdp_factory), allocatable :: matrix_data_primitives_factory
contains
    subroutine mdp_initialize(factory, options)
        class(mdp_factory), intent(in) :: factory
        type(dictionary), intent(in), optional :: options

        if ( allocated(matrix_data_primitives_factory) ) &
                error stop 'mdp_initialize::Already initialized'

        matrix_data_primitives_factory = factory
        if ( present(options) ) &
                call matrix_data_primitives_factory%set_default_options(options)
    end subroutine mdp_initialize

    subroutine mdp_finalize()

        if ( allocated(matrix_data_primitives_factory) ) then
            call matrix_data_primitives_factory%cleanup()
            deallocate(matrix_data_primitives_factory)
        end if
    end subroutine mdp_finalize
end module matrix_data_primitives_api
