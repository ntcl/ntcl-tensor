module tap_factory_module
    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter

    use :: data_api, only : &
            data_storage

    use :: tensor_arithmetic_primitives_module, only : tensor_arithmetic_primitives

    implicit none
    private

    public :: tap_factory

    type, abstract :: tap_factory
        type(dictionary) :: default_options
    contains
        procedure :: get => get
        procedure :: create => create
        procedure :: set_default_options
        procedure :: cleanup_tap_factory => cleanup_tap_factory
        procedure(create_interface), deferred :: create_from_key
        procedure(create_from_storage_interface), deferred :: create_from_storage
        procedure(get_available_interface), deferred :: get_available_tap_drivers
        procedure(empty), deferred :: cleanup
        procedure, private :: get_default_tap_driver => get_default_tap_driver
    end type tap_factory

    abstract interface
        subroutine create_interface(this, tap, key)
            import :: tap_factory
            import :: tensor_arithmetic_primitives
            import :: string

            class(tap_factory), intent(in) :: this
            class(tensor_arithmetic_primitives), allocatable, intent(inout) :: tap
            type(string), intent(in) :: key
        end subroutine create_interface

        subroutine create_from_storage_interface(this, tap, storage)
            import :: tap_factory
            import :: tensor_arithmetic_primitives
            import :: data_storage

            class(tap_factory), intent(in) :: this
            class(tensor_arithmetic_primitives), allocatable, intent(inout) :: tap
            class(data_storage), intent(in) :: storage
        end subroutine create_from_storage_interface

        function get_available_interface(this) result(drivers)
            import :: tap_factory
            import :: string

            class(tap_factory), intent(in) :: this
            type(string), dimension(:), allocatable :: drivers
        end function get_available_interface

        subroutine empty(this)
            import :: tap_factory

            class(tap_factory), intent(inout) :: this
        end subroutine empty
    end interface

    character(len=*), parameter :: tap_driver_key = "tap_driver"
    character(len=*), parameter :: default_tap_driver = "default"
contains
    function get(this, driver, options, priorities) result(tap)
        class(tap_factory), intent(in) :: this
        character(len=*), intent(in), optional :: driver
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        class(tensor_arithmetic_primitives), allocatable :: tap

        call this%create(tap, driver, options, priorities)
    end function get

    subroutine create(this, tap, driver, options, priorities)
        class(tap_factory), intent(in) :: this
        class(tensor_arithmetic_primitives), allocatable, intent(inout) :: tap
        character(len=*), intent(in), optional :: driver
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(string) :: key
        type(dictionary_converter) :: conv

        if ( present(driver) ) then
            key = driver
        else
            key = conv%to_string(string(tap_driver_key), options, priorities, this%get_default_tap_driver())
        end if
        call this%create_from_key(tap, key)
    end subroutine create

    type(string) function get_default_tap_driver(this) result(val)
        class(tap_factory), intent(in) :: this

        type(dictionary_converter) :: converter

        val = converter%to_string(tap_driver_key, this%default_options, default_value=default_tap_driver)
    end function get_default_tap_driver

    subroutine set_default_options(this, options)
        class(tap_factory), intent(inout) :: this
        type(dictionary), intent(in) :: options

        this%default_options = options
    end subroutine set_default_options

    subroutine cleanup_tap_factory(this)
        class(tap_factory), intent(inout) :: this

        call this%default_options%cleanup()
    end subroutine cleanup_tap_factory
end module tap_factory_module
