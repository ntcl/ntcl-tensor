module mdp_factory_module
    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter

    use :: tensor_package_api, only : &
            tensor

    use :: tensor_construction_api, only : &
            create_tensor_builder, &
            create_tensor_builder_from_tensor

    use :: matrix_data_primitives_module, only : matrix_data_primitives
    use :: tensor_arithmetic_primitives_api, only : &
            tensor_arithmetic_primitives_factory

    implicit none
    private

    public :: mdp_factory

    type, abstract :: mdp_factory
        type(dictionary) :: default_options
    contains
        procedure :: get => get
        procedure :: create => create
        procedure :: set_default_options
        procedure :: cleanup_mdp_factory => cleanup_mdp_factory
        procedure :: create_from_tensor => create_from_tensor
        procedure(create_interface), deferred :: create_from_key
        procedure(get_available_interface), deferred :: get_available_mdp_drivers
        procedure(empty), deferred :: cleanup
        procedure, private :: get_default_mdp_driver => get_default_mdp_driver
    end type mdp_factory

    abstract interface
        subroutine create_interface(this, mdp, key)
            import :: mdp_factory
            import :: matrix_data_primitives
            import :: string

            class(mdp_factory), intent(in) :: this
            type(matrix_data_primitives), intent(inout) :: mdp
            type(string), intent(in) :: key
        end subroutine create_interface

        function get_available_interface(this) result(drivers)
            import :: mdp_factory
            import :: string

            class(mdp_factory), intent(in) :: this
            type(string), dimension(:), allocatable :: drivers
        end function get_available_interface

        subroutine empty(this)
            import :: mdp_factory

            class(mdp_factory), intent(inout) :: this
        end subroutine empty
    end interface

    character(len=*), parameter :: mdp_driver_key = "mdp_driver"
    character(len=*), parameter :: default_mdp_driver = "default"
contains
    function get(this, driver, options, priorities) result(mdp)
        class(mdp_factory), intent(in) :: this
        character(len=*), intent(in), optional :: driver
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        type(matrix_data_primitives) :: mdp

        call this%create(mdp, driver, options, priorities)
    end function get

    subroutine create(this, mdp, driver, options, priorities)
        class(mdp_factory), intent(in) :: this
        type(matrix_data_primitives), intent(inout) :: mdp
        character(len=*), intent(in), optional :: driver
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(string) :: key
        type(dictionary_converter) :: conv

        if ( present(driver) ) then
            key = driver
        else
            key = conv%to_string( &
                    string(mdp_driver_key), options, priorities, &
                    this%get_default_mdp_driver())
        end if
        call this%create_from_key(mdp, key)
    end subroutine create

    subroutine create_from_tensor(this, mdp, atensor)
        class(mdp_factory), intent(in) :: this
        type(matrix_data_primitives), intent(inout) :: mdp
        class(tensor), intent(in) :: atensor

        call create_tensor_builder_from_tensor(mdp%builder, atensor)
        call tensor_arithmetic_primitives_factory%create_from_storage( &
                mdp%tap, atensor%storage)
    end subroutine create_from_tensor


    type(string) function get_default_mdp_driver(this) result(val)
        class(mdp_factory), intent(in) :: this

        type(dictionary_converter) :: converter

        val = converter%to_string(mdp_driver_key, this%default_options, &
                default_value=default_mdp_driver)
    end function get_default_mdp_driver

    subroutine set_default_options(this, options)
        class(mdp_factory), intent(inout) :: this
        type(dictionary), intent(in) :: options

        this%default_options = options
    end subroutine set_default_options

    subroutine cleanup_mdp_factory(this)
        class(mdp_factory), intent(inout) :: this

        call this%default_options%cleanup()
    end subroutine cleanup_mdp_factory
end module mdp_factory_module
