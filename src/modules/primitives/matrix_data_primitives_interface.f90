module matrix_data_primitives_interface
    use :: util_api, only : &
            tile

    use :: data_api, only : &
            stream

    use :: tensor_package_api, only : &
            matrix, &
            scalar

    implicit none
    private

    public :: add_tiled_matrix_data
contains
    subroutine add_tiled_matrix_data(dst, dst_tile, src, src_tile, &
            alpha, beta, astream)
        type(matrix), intent(inout) :: dst
        type(tile), intent(in) :: dst_tile
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: src_tile
        type(scalar), intent(in), optional :: alpha, beta
        type(stream), intent(in), optional :: astream

        error stop 'add_tiled_matrix_data::Not implemented.'
    end subroutine add_tiled_matrix_data
end module matrix_data_primitives_interface
