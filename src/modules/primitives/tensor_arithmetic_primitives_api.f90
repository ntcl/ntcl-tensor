module tensor_arithmetic_primitives_api
    use, intrinsic :: iso_fortran_env, only : &
            real32, &
            real64

    use :: util_api, only : &
            dictionary

    use :: data_api, only : &
            stream, &
            memory_manager

    use :: tensor_package_api, only : &
            scalar, &
            tensor

    use :: tensor_arithmetic_primitives_module, only : tensor_arithmetic_primitives
    use :: tap_factory_module, only : tap_factory

    implicit none
    private

    public :: tensor_arithmetic_primitives
    public :: tap_factory

    public :: tensor_arithmetic_primitives_factory

    public :: tap_initialize
    public :: tap_finalize

    public :: assignment(=)
    public :: scalar_tensor_multiplication

    public :: copy_tensor_data
    public :: add_tensor_data

    class(tap_factory), allocatable :: tensor_arithmetic_primitives_factory

    interface assignment(=)
        module procedure assign_integer
        module procedure assign_real32
        module procedure assign_real64
        module procedure assign_complex64
        module procedure assign_complex128
        module procedure assign_scalar
    end interface assignment(=)
contains
    subroutine tap_initialize(factory, options)
        class(tap_factory), intent(in) :: factory
        type(dictionary), intent(in), optional :: options

        if ( allocated(tensor_arithmetic_primitives_factory) ) &
                error stop 'tap_initialize::Already initalized.'

        tensor_arithmetic_primitives_factory = factory
        if ( present(options) ) &
            call tensor_arithmetic_primitives_factory%set_default_options(options)
    end subroutine tap_initialize

    subroutine tap_finalize()

        if ( allocated(tensor_arithmetic_primitives_factory) ) then
            call tensor_arithmetic_primitives_factory%cleanup()
            deallocate(tensor_arithmetic_primitives_factory)
        end if
    end subroutine tap_finalize

    subroutine assign_integer(dst, alpha)
        class(tensor), intent(inout) :: dst
        integer, intent(in) :: alpha

        dst = scalar(alpha)
    end subroutine assign_integer

    subroutine assign_real32(dst, alpha)
        class(tensor), intent(inout) :: dst
        real(real32), intent(in) :: alpha

        dst = scalar(alpha)
    end subroutine assign_real32

    subroutine assign_real64(dst, alpha)
        class(tensor), intent(inout) :: dst
        real(real64), intent(in) :: alpha

        dst = scalar(alpha)
    end subroutine assign_real64

    subroutine assign_complex64(dst, alpha)
        class(tensor), intent(inout) :: dst
        complex(real32), intent(in) :: alpha

        dst = scalar(alpha)
    end subroutine assign_complex64

    subroutine assign_complex128(dst, alpha)
        class(tensor), intent(inout) :: dst
        complex(real64), intent(in) :: alpha

        dst = scalar(alpha)
    end subroutine assign_complex128

    subroutine assign_scalar(dst, alpha)
        class(tensor), intent(inout) :: dst
        type(scalar), intent(in) :: alpha

        class(tensor_arithmetic_primitives), allocatable :: tap

        call tensor_arithmetic_primitives_factory%create_from_storage( &
                tap, dst%storage)

        call tap%scalar_set(dst, alpha)

        call tap%cleanup()
        deallocate(tap)
    end subroutine assign_scalar

    subroutine scalar_tensor_multiplication(dst, alpha)
        class(tensor), intent(inout) :: dst
        type(scalar), intent(in) :: alpha

        class(tensor_arithmetic_primitives), allocatable :: tap

        call tensor_arithmetic_primitives_factory%create_from_storage( &
                tap, dst%storage)

        call tap%scalar_multiply(dst, alpha)

        call tap%cleanup()
        deallocate(tap)
    end subroutine scalar_tensor_multiplication

    subroutine copy_tensor_data(dst, src, astream)
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        type(stream), optional :: astream

        call memory_manager%copy_storage(src%storage, dst%storage, astream)
    end subroutine copy_tensor_data

    subroutine add_tensor_data(dst, src, alpha, astream)
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        type(scalar), intent(in) :: alpha
        type(stream), optional :: astream

        class(tensor_arithmetic_primitives), allocatable :: tap

        call tensor_arithmetic_primitives_factory%create_from_storage( &
                tap, dst%storage)

        call tap%add(dst, src, alpha, astream)

        call tap%cleanup()
        deallocate(tap)
    end subroutine add_tensor_data
end module tensor_arithmetic_primitives_api
