module matrix_data_primitives_module
    use, intrinsic :: iso_fortran_env, only : &
            int64

    use :: util_api, only : &
            tile

    use :: data_api, only : &
            scratch_buffer, &
            stream, &
            memory_manager

    use :: tensor_package_api, only : &
            tensor, &
            scalar, &
            matrix, &
            tensor_datatype_helper

    use :: tensor_construction_api, only : &
            tensor_builder

    use :: tensor_arithmetic_primitives_module, only : &
            tensor_arithmetic_primitives

    implicit none
    private

    public :: matrix_data_primitives

    type :: matrix_data_primitives
        type(tensor_builder) :: builder
        class(tensor_arithmetic_primitives), allocatable :: tap
    contains
        procedure :: allocate_and_copy_tile => allocate_and_copy_tile
        procedure :: copy_tile => copy_tile
        procedure :: copy_tile_and_reshape => copy_tile_and_reshape
        procedure :: copy_tile_to_scratch => copy_tile_to_scratch
        procedure :: copy_tile_and_reshape_to_scratch => copy_tile_and_reshape_to_scratch
        procedure :: set_tile => set_tile
        procedure :: update_tile => update_tile
        procedure :: update_tile_using_scratch => update_tile_using_scratch
        procedure :: reshape_and_set_tile => reshape_and_set_tile
        procedure :: reshape_and_update_tile => reshape_and_update_tile
        procedure :: reshape_and_update_tile_using_scratch => reshape_and_update_tile_using_scratch
        procedure :: update => update
        procedure :: update_using_scratch => update_using_scratch
        procedure :: set => set
        procedure :: cleanup => cleanup
    end type matrix_data_primitives

    interface matrix_data_primitives
        module procedure constructor
    end interface matrix_data_primitives
contains
    function constructor() result(this)
        type(matrix_data_primitives) :: this

    end function constructor

    subroutine cleanup(this)
        class(matrix_data_primitives), intent(inout) :: this

        call this%builder%cleanup()
        if ( allocated(this%tap) ) then
            call this%tap%cleanup()
            deallocate(this%tap)
        end if
    end subroutine cleanup

    subroutine allocate_and_copy_tile(this, dst, src, src_tile, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: src_tile
        type(stream), intent(in), optional :: astream

        call this%builder%create(dst, src%datatype, src_tile%all_sizes, .false.)
        call this%copy_tile(dst, src, src_tile, astream)
    end subroutine allocate_and_copy_tile

    subroutine copy_tile_to_scratch(this, dst, scratch, src, src_tile, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(scratch_buffer), intent(inout) :: scratch
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: src_tile
        type(stream), intent(in), optional :: astream

        call this%builder%create_in_scratch(dst, scratch, src%datatype, src_tile%all_sizes, .false.)
        call this%copy_tile(dst, src, src_tile, astream)
    end subroutine copy_tile_to_scratch

    subroutine copy_tile(this, dst, src, src_tile, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: src_tile
        type(stream), intent(in), optional :: astream

        call this%set(dst, tile(1, dst%dims(1), 1, dst%dims(2)), src, src_tile, astream)
    end subroutine copy_tile

    subroutine set_tile(this, dst, dst_tile, src, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(tile), intent(in) :: dst_tile
        type(matrix), intent(in) :: src
        type(stream), intent(in), optional :: astream

        call this%set(dst, dst_tile, src, tile(1, src%dims(1), 1, src%dims(2)), astream)
    end subroutine set_tile

    subroutine update_tile(this, dst, dst_tile, src, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(tile), intent(in) :: dst_tile
        type(matrix), intent(in) :: src
        type(stream), intent(in), optional :: astream

        call this%update(dst, dst_tile, src, tile(1, src%dims(1), 1, src%dims(2)), &
                scalar(1), scalar(1), astream)
    end subroutine update_tile

    subroutine update_tile_using_scratch(this, dst, scratch, dst_tile, src, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(scratch_buffer), intent(inout) :: scratch
        type(tile), intent(in) :: dst_tile
        type(matrix), intent(in) :: src
        type(stream), intent(in), optional :: astream

        call this%update_using_scratch(dst, scratch, dst_tile, src, tile(1, src%dims(1), 1, src%dims(2)), &
                scalar(1), scalar(1), astream)
    end subroutine update_tile_using_scratch

    subroutine copy_tile_and_reshape(this, dst, dst_shape, src, src_tile, astream)
        class(matrix_data_primitives), intent(inout) :: this
        class(tensor), allocatable, intent(inout) :: dst
        integer(int64), dimension(:), intent(in) :: dst_shape
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: src_tile
        type(stream), intent(in), optional :: astream

        type(matrix) :: tmp

        tmp = matrix()

        call this%builder%allocate_and_create(dst, src%datatype, dst_shape, .false.)
        call this%builder%reshape(tmp, dst, src_tile%all_sizes)
        call this%copy_tile(tmp, src, src_tile, astream)
        call tmp%release()
    end subroutine copy_tile_and_reshape

    subroutine copy_tile_and_reshape_to_scratch(this, dst, scratch, dst_shape, src, src_tile, astream)
        class(matrix_data_primitives), intent(inout) :: this
        class(tensor), allocatable, intent(inout) :: dst
        type(scratch_buffer), intent(inout) :: scratch
        integer(int64), dimension(:), intent(in) :: dst_shape
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: src_tile
        type(stream), intent(in), optional :: astream

        type(matrix) :: tmp

        tmp = matrix()

        call this%builder%allocate_and_create_in_scratch(dst, scratch, src%datatype, dst_shape, .false.)
        call this%builder%reshape(tmp, dst, src_tile%all_sizes)
        call this%copy_tile(tmp, src, src_tile, astream)
        call tmp%release()
    end subroutine copy_tile_and_reshape_to_scratch

    subroutine reshape_and_set_tile(this, dst, dst_tile, src, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(tile), intent(in) :: dst_tile
        class(tensor), intent(in) :: src
        type(stream), intent(in), optional :: astream

        type(matrix) :: tmp

        tmp = matrix()

        call this%builder%reshape(tmp, src, dst_tile%all_sizes)
        call this%set_tile(dst, dst_tile, tmp, astream)
        call tmp%release()
    end subroutine reshape_and_set_tile

    subroutine reshape_and_update_tile(this, dst, dst_tile, src, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(tile), intent(in) :: dst_tile
        class(tensor), intent(in) :: src
        type(stream), intent(in), optional :: astream

        type(matrix) :: tmp

        tmp = matrix()

        call this%builder%reshape(tmp, src, dst_tile%all_sizes)
        call this%update(dst, dst_tile, tmp, tile(1, tmp%dims(1), 1, tmp%dims(2)), &
                scalar(1), scalar(1), astream)
    end subroutine reshape_and_update_tile

    subroutine reshape_and_update_tile_using_scratch(this, dst, scratch, dst_tile, src, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(scratch_buffer), intent(inout) :: scratch
        type(tile), intent(in) :: dst_tile
        class(tensor), intent(in) :: src
        type(stream), intent(in), optional :: astream

        type(matrix) :: tmp

        tmp = matrix()

        call this%builder%reshape(tmp, src, dst_tile%all_sizes)
        call this%update_using_scratch(dst, scratch, dst_tile, tmp, tile(1, tmp%dims(1), 1, tmp%dims(2)), &
                scalar(1), scalar(1), astream)
    end subroutine reshape_and_update_tile_using_scratch

    subroutine update(this, dst, dtile, src, stile, alpha, beta, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(tile), intent(in) :: dtile
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: stile
        type(scalar), intent(in) :: alpha, beta
        type(stream), intent(in), optional :: astream

        type(matrix) :: msrc, mdst

        if ( beta%is_zero() .and. alpha%is_one() ) then
            call this%set(dst, dtile, src, stile, astream)
        else if ( beta%is_zero() ) then
            call this%allocate_and_copy_tile(mdst, src, stile, astream)
            call this%tap%scalar_multiply(mdst, alpha, astream)
            call this%set_tile(dst, dtile, mdst, astream)
        else
            call this%allocate_and_copy_tile(mdst, dst, dtile, astream)
            call this%allocate_and_copy_tile(msrc, src, stile, astream)
            call this%tap%scalar_multiply(mdst, beta, astream)
            call this%tap%add(mdst, msrc, alpha, astream)
            call this%set_tile(dst, dtile, mdst, astream)
        end if

        call mdst%cleanup()
        call msrc%cleanup()
    end subroutine update

    subroutine update_using_scratch(this, dst, scratch, dtile, src, stile, alpha, beta, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(scratch_buffer), intent(inout) :: scratch
        type(tile), intent(in) :: dtile
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: stile
        type(scalar), intent(in) :: alpha, beta
        type(stream), intent(in), optional :: astream

        type(matrix) :: msrc, mdst

        if ( beta%is_zero() .and. alpha%is_one() ) then
            call this%set(dst, dtile, src, stile, astream)
        else if ( beta%is_zero() ) then
            call this%copy_tile_to_scratch(mdst, scratch, src, stile, astream)
            call this%tap%scalar_multiply(mdst, alpha, astream)
            call this%set_tile(dst, dtile, mdst, astream)
            call scratch%destroy(mdst%storage)
        else
            call this%copy_tile_to_scratch(mdst, scratch, dst, dtile, astream)
            call this%copy_tile_to_scratch(msrc, scratch, src, stile, astream)
            call this%tap%scalar_multiply(mdst, beta, astream)
            call this%tap%add(mdst, msrc, alpha, astream)
            call this%set_tile(dst, dtile, mdst, astream)
            call scratch%destroy(msrc%storage)
            call scratch%destroy(mdst%storage)
        end if

        call mdst%release()
        call msrc%release()
    end subroutine update_using_scratch

    subroutine set(this, dst, dtile, src, stile, astream)
        class(matrix_data_primitives), intent(inout) :: this
        type(matrix), intent(inout) :: dst
        type(tile), intent(in) :: dtile
        type(matrix), intent(in) :: src
        type(tile), intent(in) :: stile
        type(stream), intent(in), optional :: astream

        type(tile) :: dst_tile, src_tile
        integer :: number_of_bytes_per_element
        integer(int64), dimension(2) :: src_dims, dst_dims
        type(tensor_datatype_helper) :: helper

        number_of_bytes_per_element = helper%get_datatype_size(dst%datatype)

        dst_tile = tile(&
                dtile%row_offset*number_of_bytes_per_element+1, &
                dtile%row_offset*number_of_bytes_per_element + dtile%row_size*number_of_bytes_per_element, &
                dtile%col_offset+1, &
                dtile%col_offset + dtile%col_size)

        src_tile = tile(&
                stile%row_offset*number_of_bytes_per_element+1, &
                stile%row_offset*number_of_bytes_per_element + stile%row_size*number_of_bytes_per_element, &
                stile%col_offset+1, &
                stile%col_offset + stile%col_size)

        dst_dims = [dst%dims(1)*number_of_bytes_per_element, dst%dims(2)]
        src_dims = [src%dims(1)*number_of_bytes_per_element, src%dims(2)]

        call memory_manager%copy_2d(dst%storage, dst_dims, dst_tile, src%storage, src_dims, src_tile, astream)
    end subroutine set
end module matrix_data_primitives_module
