module tensor_construction_api
    use :: tensor_builder_module, only : tensor_builder
    use :: tensor_builder_factory_module, only : &
            get_tensor_builder, &
            create_tensor_builder, &
            create_tensor_builder_from_tensor

    use :: tensor_builder_interface

    implicit none
    public
end module tensor_construction_api
