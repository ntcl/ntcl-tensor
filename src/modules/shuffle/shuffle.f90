module shuffle_module
    use, intrinsic :: iso_fortran_env, only : &
            int64

    use :: util_api, only : &
            slice

    use :: data_api, only : &
            memory_manager, &
            stream

    use :: tensor_module, only : &
            tensor

    use :: tensor_datatype_helper_module, only : &
            tensor_datatype_helper

    implicit none
    private

    public :: shuffle

    type, abstract :: shuffle
    contains
        generic :: execute => &
                execute_straight_copy, &
                execute_sliced_copy, &
                execute_random_copy, &
                execute_random_copy_int32

        procedure :: execute_straight_copy => execute_straight_copy
        procedure :: execute_sliced_copy => execute_sliced_copy
        procedure :: execute_random_copy_int32 => execute_random_copy_int32

        procedure(random_copy), deferred :: execute_random_copy
        procedure(synchronize_interface), deferred :: synchronize
        procedure(empty), deferred :: cleanup

        procedure, private :: get_storage_slice => get_storage_slice
    end type shuffle

    abstract interface
        subroutine random_copy(this, dst, src, didx, sidx, astream)
            import :: shuffle
            import :: tensor
            import :: int64
            import :: stream

            class(shuffle), intent(inout) :: this
            class(tensor), intent(inout) :: dst
            class(tensor), intent(in) :: src
            integer(int64), dimension(:), intent(in) :: didx, sidx
            type(stream), intent(in), optional :: astream
        end subroutine random_copy

        subroutine synchronize_interface(this, astream)
            import :: shuffle
            import :: stream

            class(shuffle), intent(in) :: this
            type(stream), intent(in), optional :: astream
        end subroutine synchronize_interface

        subroutine empty(this)
            import :: shuffle

            class(shuffle), intent(inout) :: this
        end subroutine empty
    end interface
contains
    subroutine execute_straight_copy(this, dst, src, astream)
        class(shuffle), intent(in) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        type(stream), intent(in), optional :: astream

        call memory_manager%copy_storage(src%storage, dst%storage, astream)
    end subroutine execute_straight_copy

    subroutine execute_sliced_copy(this, dst, src, dst_slice, src_slice, astream)
        class(shuffle), intent(in) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        type(slice), intent(in) :: dst_slice, src_slice
        type(stream), intent(in), optional :: astream

        call memory_manager%copy_slice(&
                dst%storage, this%get_storage_slice(dst, dst_slice), &
                src%storage, this%get_storage_slice(src, src_slice), astream)
    end subroutine execute_sliced_copy

    subroutine execute_random_copy_int32(this, dst, src, didx, sidx, astream)
        class(shuffle), intent(inout) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        integer, dimension(:), intent(in) :: didx, sidx
        type(stream), intent(in), optional :: astream

        call this%execute(dst, src, int(didx, int64), int(sidx, int64), astream)
    end subroutine execute_random_copy_int32

    type(slice) function get_storage_slice(this, atensor, aslice)
        class(shuffle), intent(in) :: this
        class(tensor), intent(in) :: atensor
        type(slice), intent(in) :: aslice

        integer :: datatype_size
        type(tensor_datatype_helper) :: helper

        datatype_size = helper%get_datatype_size(atensor%datatype)

        get_storage_slice = slice( &
                aslice%offset*datatype_size + 1, &
                aslice%last*datatype_size)
    end function get_storage_slice
end module shuffle_module
