module abstract_shuffle_factory_module
    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter

    use :: shuffle_module, only : &
            shuffle

    implicit none
    private

    public :: abstract_shuffle_factory

    type, abstract :: abstract_shuffle_factory
    contains
        procedure :: get => get
        procedure :: create => create
        procedure(create_interface), deferred :: create_from_key
        procedure(build_interface), deferred :: build
        procedure(drivers), deferred :: get_available_drivers
        procedure(empty), deferred :: cleanup
    end type abstract_shuffle_factory

    abstract interface
        subroutine create_interface(this, ashuffle, key)
            import :: abstract_shuffle_factory
            import :: shuffle
            import :: string

            class(abstract_shuffle_factory), intent(in) :: this
            class(shuffle), allocatable, intent(inout) :: ashuffle
            type(string), intent(in) :: key
        end subroutine create_interface

        subroutine build_interface(this, driver, options, priorities)
            import :: abstract_shuffle_factory
            import :: shuffle
            import :: dictionary
            import :: string

            class(abstract_shuffle_factory), intent(in) :: this
            class(shuffle), intent(inout) :: driver
            type(dictionary), intent(in), optional :: options
            type(string), dimension(:), intent(in), optional :: priorities
        end subroutine build_interface

        function drivers(this) result(res)
            import :: abstract_shuffle_factory
            import :: string

            class(abstract_shuffle_factory), intent(in) :: this
            type(string), dimension(:), allocatable :: res
        end function drivers

        subroutine empty(this)
            import :: abstract_shuffle_factory

            class(abstract_shuffle_factory), intent(inout) :: this
        end subroutine empty
    end interface

    character(len=*), parameter :: shuffle_driver_key = "shuffle_driver"
    character(len=*), parameter :: default_shuffle_driver = "intrinsic"
contains
    function get(this, driver, options, priorities) result(ashuffle)
        class(abstract_shuffle_factory), intent(in) :: this
        character(len=*), intent(in), optional :: driver
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        class(shuffle), allocatable :: ashuffle

        call this%create(ashuffle, driver, options, priorities)
    end function get

    subroutine create(this, ashuffle, driver, options, priorities)
        class(abstract_shuffle_factory), intent(in) :: this
        class(shuffle), allocatable, intent(inout) :: ashuffle
        character(len=*), intent(in), optional :: driver
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(string) :: key
        type(dictionary_converter) :: conv

        if ( present(driver) ) then
            key = driver
        else
            key = conv%to_string(shuffle_driver_key, options, priorities, default_shuffle_driver)
        end if
        call this%create_from_key(ashuffle, key)
        call this%build(ashuffle, options, priorities)
    end subroutine create

end module abstract_shuffle_factory_module
