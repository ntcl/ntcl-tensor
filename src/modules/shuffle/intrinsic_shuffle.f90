module intrinsic_shuffle_module
    use, intrinsic :: iso_fortran_env, only : &
            int64, &
            real32, &
            real64

    use :: data_api, only : &
            stream

    use :: tensor_package_api, only : &
            tensor, &
            dt_c128, &
            dt_c64, &
            dt_r64, &
            dt_r32

    use :: tensor_fortran_converter_module, only : &
            secure_fortran_pointer_from_tensor, &
            release_pointer_from_remote_tensor, &
            update_remote_tensor_and_release_pointer

    use :: shuffle_module, only : shuffle

    implicit none
    private

    public :: intrinsic_shuffle

    type, extends(shuffle) :: intrinsic_shuffle
    contains
        procedure :: execute_random_copy => execute_random_copy
        procedure :: synchronize => synchronize
        procedure :: cleanup => cleanup
        procedure :: clear => clear

        procedure, private :: execute_random_copy_r32 => execute_random_copy_r32
        procedure, private :: execute_random_copy_r64 => execute_random_copy_r64
        procedure, private :: execute_random_copy_c64 => execute_random_copy_c64
        procedure, private :: execute_random_copy_c128 => execute_random_copy_c128
    end type intrinsic_shuffle

    interface intrinsic_shuffle
        module procedure constructor
    end interface intrinsic_shuffle

contains
    function constructor() result(this)
        type(intrinsic_shuffle) :: this

        call this%clear()
    end function constructor

    subroutine execute_random_copy(this, dst, src, didx, sidx, astream)
        class(intrinsic_shuffle), intent(inout) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        integer(int64), dimension(:), intent(in) :: didx, sidx
        type(stream), intent(in), optional :: astream

        select case (dst%datatype)
        case (dt_r32)
            call this%execute_random_copy_r32(dst, src, didx, sidx, astream)
        case (dt_r64)
            call this%execute_random_copy_r64(dst, src, didx, sidx, astream)
        case (dt_c64)
            call this%execute_random_copy_c64(dst, src, didx, sidx, astream)
        case (dt_c128)
            call this%execute_random_copy_c128(dst, src, didx, sidx, astream)
        case default
            error stop 'intrinsic_shuffle::execute_random_copy:Datatype not supported.'
        end select
    end subroutine execute_random_copy

    subroutine synchronize(this, astream)
        class(intrinsic_shuffle), intent(in) :: this
        type(stream), intent(in), optional :: astream

        continue ! Nothing do be done
    end subroutine synchronize

    subroutine execute_random_copy_r32(this, dst, src, didx, sidx, astream)
        class(intrinsic_shuffle), intent(in) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        integer(int64), dimension(:), intent(in) :: didx, sidx
        type(stream), intent(in), optional :: astream

        real(real32), dimension(:), pointer, contiguous :: d_ptr, s_ptr
        integer(int64) :: idx

        call secure_fortran_pointer_from_tensor(d_ptr, dst, astream)
        call secure_fortran_pointer_from_tensor(s_ptr, src, astream)

        do idx = 1, size(didx)
            d_ptr(didx(idx)) = s_ptr(sidx(idx))
        end do

        call update_remote_tensor_and_release_pointer(d_ptr, dst, astream)
        call release_pointer_from_remote_tensor(s_ptr, src, astream)
    end subroutine execute_random_copy_r32

    subroutine execute_random_copy_r64(this, dst, src, didx, sidx, astream)
        class(intrinsic_shuffle), intent(in) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        integer(int64), dimension(:), intent(in) :: didx, sidx
        type(stream), intent(in), optional :: astream

        real(real64), dimension(:), pointer, contiguous :: d_ptr, s_ptr
        integer(int64) :: idx

        call secure_fortran_pointer_from_tensor(d_ptr, dst, astream)
        call secure_fortran_pointer_from_tensor(s_ptr, src, astream)

        do idx = 1, size(didx)
            d_ptr(didx(idx)) = s_ptr(sidx(idx))
        end do

        call update_remote_tensor_and_release_pointer(d_ptr, dst, astream)
        call release_pointer_from_remote_tensor(s_ptr, src, astream)
    end subroutine execute_random_copy_r64

    subroutine execute_random_copy_c64(this, dst, src, didx, sidx, astream)
        class(intrinsic_shuffle), intent(in) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        integer(int64), dimension(:), intent(in) :: didx, sidx
        type(stream), intent(in), optional :: astream

        complex(real32), dimension(:), pointer, contiguous :: d_ptr, s_ptr
        integer(int64) :: idx

        call secure_fortran_pointer_from_tensor(d_ptr, dst, astream)
        call secure_fortran_pointer_from_tensor(s_ptr, src, astream)

        do idx = 1, size(didx)
            d_ptr(didx(idx)) = s_ptr(sidx(idx))
        end do

        call update_remote_tensor_and_release_pointer(d_ptr, dst, astream)
        call release_pointer_from_remote_tensor(s_ptr, src, astream)
    end subroutine execute_random_copy_c64

    subroutine execute_random_copy_c128(this, dst, src, didx, sidx, astream)
        class(intrinsic_shuffle), intent(in) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        integer(int64), dimension(:), intent(in) :: didx, sidx
        type(stream), intent(in), optional :: astream

        complex(real64), dimension(:), pointer, contiguous :: d_ptr, s_ptr
        integer(int64) :: idx

        call secure_fortran_pointer_from_tensor(d_ptr, dst, astream)
        call secure_fortran_pointer_from_tensor(s_ptr, src, astream)

        do idx = 1, size(didx)
            d_ptr(didx(idx)) = s_ptr(sidx(idx))
        end do

        call update_remote_tensor_and_release_pointer(d_ptr, dst, astream)
        call release_pointer_from_remote_tensor(s_ptr, src, astream)
    end subroutine execute_random_copy_c128

    subroutine cleanup(this)
        class(intrinsic_shuffle), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(intrinsic_shuffle), intent(inout) :: this
    end subroutine clear
end module intrinsic_shuffle_module
