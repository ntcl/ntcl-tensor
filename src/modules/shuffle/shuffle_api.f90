module shuffle_api
    use :: shuffle_module, only : shuffle

    use :: abstract_shuffle_factory_module, only : &
            abstract_shuffle_factory


    implicit none
    private

    public :: shuffle

    public :: shuffle_factory

    public :: shuffle_initialize
    public :: shuffle_finalize

    class(abstract_shuffle_factory), allocatable :: shuffle_factory
contains
    subroutine shuffle_initialize(factory)
        class(abstract_shuffle_factory), intent(in) :: factory

        shuffle_factory = factory
    end subroutine shuffle_initialize

    subroutine shuffle_finalize()
        if ( allocated(shuffle_factory) ) then
            call shuffle_factory%cleanup()
            deallocate(shuffle_factory)
        end if
    end subroutine shuffle_finalize
end module shuffle_api
