module tensor_dev
    use :: tensor_api

    use :: shuffle_dev

    use :: default_tap_factory_module, only : &
            default_tap_factory

    use :: default_mdp_factory_module, only : &
            default_mdp_factory

    use :: default_shuffle_factory_module, only : &
            default_shuffle_factory

    implicit none
    public
end module tensor_dev
