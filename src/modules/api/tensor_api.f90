module tensor_api
    use :: tensor_package_api
    use :: tensor_converter_factory_module, only : &
            tensor_converter_factory
    use :: tensor_c_pointer_converter_module, only : tensor_c_pointer_converter
    use :: tensor_fortran_converter_module, only : &
            tensor_fortran_converter, &
            secure_fortran_pointer_from_tensor, &
            update_remote_tensor_from_pointer, &
            release_pointer_from_remote_tensor, &
            update_remote_tensor_and_release_pointer

    use :: tensor_construction_api
    use :: tensor_initializer_api
    use :: tensor_arithmetic_primitives_api
    use :: matrix_data_primitives_api
    use :: shuffle_api

    implicit none
    public
end module tensor_api
