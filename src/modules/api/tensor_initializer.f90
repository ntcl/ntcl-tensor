module tensor_initializer
    use :: util_api, only : &
            dictionary

    use :: data_initializer, only : &
            data_initialize, &
            data_finalize

    use :: tensor_dev, only : &
            tap_initialize, &
            tap_finalize, &
            default_tap_factory, &
            mdp_initialize, &
            mdp_finalize, &
            default_mdp_factory, &
            shuffle_initialize, &
            shuffle_finalize, &
            default_shuffle_factory

    implicit none
    public

contains
    subroutine tensor_initialize(options)
        type(dictionary), intent(in), optional :: options

        call data_initialize(options)

        call tap_initialize(default_tap_factory(), options)
        call mdp_initialize(default_mdp_factory(), options)
        call shuffle_initialize(default_shuffle_factory())
    end subroutine

    subroutine tensor_finalize()

        call shuffle_finalize()
        call mdp_finalize()
        call tap_finalize()

        call data_finalize()
    end subroutine tensor_finalize
end module tensor_initializer
