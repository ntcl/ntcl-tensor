module tensor_rankn_module
    use, intrinsic :: iso_fortran_env, only : int64
    use :: data_storage_module, only : data_storage

    ! All public objects in tensor are public here
    use :: tensor_module

    implicit none
    public

    type, extends(tensor) :: tensor_rankn
    end type tensor_rankn

    interface tensor_rankn
        module procedure constructor_empty
        module procedure constructor_int64_array
    end interface tensor_rankn
contains
    function constructor_empty(rank) result(this)
        integer, intent(in) :: rank
        type(tensor_rankn) :: this

        call this%clear()
        this%rank = rank
    end function constructor_empty

    function constructor_int64_array(storage, datatype, dims) result(this)
        class(data_storage), intent(in) :: storage
        integer, intent(in) :: datatype
        integer(int64), dimension(5), intent(in) :: dims
        type(tensor_rankn) :: this

        this = tensor_rankn(size(dims))
        call this%setup(storage, datatype, dims)
    end function constructor_int64_array
end module tensor_rankn_module
