module matrix_view_module
    use, intrinsic :: iso_fortran_env, only : int64, int32

    use :: tile_module, only : tile

    implicit none
    private

    public :: matrix_view

    type :: matrix_view
        integer(int64) :: row_offset
        integer(int64) :: row_size
        integer(int64) :: col_offset
        integer(int64) :: col_size
    end type matrix_view

    interface matrix_view
        module procedure constructor
        module procedure constructor_from_tile
        module procedure constructor_int32
        module procedure constructor_int64
        module procedure constructor_2int64_2int32
        module procedure constructor_2int64_2int32_v2
    end interface matrix_view
contains
    pure function constructor() result(amatrix_view)
        type(matrix_view) :: amatrix_view

        amatrix_view%row_offset = 0
        amatrix_view%col_offset = 0

        amatrix_view%row_size = 0
        amatrix_view%col_size = 0
    end function constructor

    pure function constructor_from_tile(atile) result(amatrix_view)
        type(tile), intent(in) :: atile
        type(matrix_view) :: amatrix_view

        amatrix_view%row_offset = atile%row_offset
        amatrix_view%col_offset = atile%col_offset

        amatrix_view%row_size = atile%row_size
        amatrix_view%col_size = atile%col_size
    end function constructor_from_tile

    pure function constructor_int64(ro, rn, co, cn) result(amatrix_view)
        integer(int64), intent(in) :: ro
        integer(int64), intent(in) :: rn
        integer(int64), intent(in) :: co
        integer(int64), intent(in) :: cn
        type(matrix_view) :: amatrix_view

        amatrix_view%row_offset = ro
        amatrix_view%col_offset = co

        amatrix_view%row_size = rn
        amatrix_view%col_size = cn
    end function constructor_int64

    pure function constructor_int32(ro, rn, co, cn) result(amatrix_view)
       integer(int32), intent(in) :: ro
        integer(int32), intent(in) :: rn
        integer(int32), intent(in) :: co
        integer(int32), intent(in) :: cn
        type(matrix_view) :: amatrix_view

        amatrix_view = constructor_int64(int(ro, int64), int(rn, int64), int(co, int64), int(cn, int64))
    end function constructor_int32

    pure function constructor_2int64_2int32(ro, rn, co, cn) result(amatrix_view)
        integer(int64), intent(in) :: ro
        integer(int64), intent(in) :: rn
        integer(int32), intent(in) :: co
        integer(int32), intent(in) :: cn
        type(matrix_view) :: amatrix_view

        amatrix_view = constructor_int64(int(ro, int64), int(rn, int64), int(co, int64), int(cn, int64))
    end function constructor_2int64_2int32

    pure function constructor_2int64_2int32_v2(ro, rn, co, cn) result(amatrix_view)
        integer(int32), intent(in) :: ro
        integer(int64), intent(in) :: rn
        integer(int32), intent(in) :: co
        integer(int64), intent(in) :: cn
        type(matrix_view) :: amatrix_view

        amatrix_view = constructor_int64(int(ro, int64), int(rn, int64), int(co, int64), int(cn, int64))
    end function constructor_2int64_2int32_v2
end module matrix_view_module
