module tensor_slice_module
    use, intrinsic :: iso_fortran_env, only : int64

    implicit none
    private

    public :: tensor_slice

    type :: tensor_slice
        integer(int64), dimension(:), allocatable :: offsets, sizes
    contains
        generic :: operator (.eq.) => is_equal
        generic :: operator (.ne.) => is_not_equal
        procedure :: get_first => get_first
        procedure :: get_last => get_last
        procedure :: is_equal => is_equal
        procedure :: is_not_equal => is_not_equal
        procedure :: union => union
        procedure :: cleanup => cleanup
    end type tensor_slice

    interface tensor_slice
        module procedure constructor
        module procedure constructor_no_offset
    end interface tensor_slice
contains
    function constructor(offsets, sizes) result(this)
        integer(int64), dimension(:), intent(in) :: offsets, sizes
        type(tensor_slice) :: this

        this%offsets = offsets
        this%sizes = sizes
    end function constructor

    function constructor_no_offset(sizes) result(this)
        integer(int64), dimension(:), intent(in) :: sizes
        type(tensor_slice) :: this

        this%offsets = sizes
        this%sizes = sizes
        this%offsets(:) = 0
    end function constructor_no_offset

    function get_first(this) result(first)
        class(tensor_slice), intent(in) :: this
        integer(int64), dimension(:), allocatable :: first

        first = this%offsets + 1_int64
    end function get_first

    function get_last(this) result(last)
        class(tensor_slice), intent(in) :: this
        integer(int64), dimension(:), allocatable :: last

        last = this%offsets + this%sizes
    end function get_last

    type(tensor_slice) function union(this, other)
        class(tensor_slice), intent(in) :: this, other

        integer(int64), dimension(:), allocatable :: offsets, sizes

        offsets = min(this%offsets, other%offsets)
        sizes  = max(this%offsets+this%sizes, other%offsets + other%sizes) - offsets
        union = tensor_slice(offsets, sizes)
    end function union

    logical function is_equal(this, other)
        class(tensor_slice), intent(in) :: this, other

        is_equal = &
                ( allocated(this%offsets) .eqv. allocated(other%offsets)) .and. &
                ( allocated(this%sizes) .eqv. allocated(other%sizes))

        if (.not. is_equal) return

        if ( allocated(this%offsets) ) &
                is_equal = is_equal .and. &
                        size(this%offsets) == size(other%offsets) .and. &
                        all(this%offsets == other%offsets)

        if ( allocated(this%sizes) ) &
                is_equal = is_equal .and. &
                        size(this%sizes) == size(other%sizes) .and. &
                        all(this%sizes == other%sizes)
    end function is_equal

    logical function is_not_equal(this, other)
        class(tensor_slice), intent(in) :: this, other

        is_not_equal = .not. this == other
    end function is_not_equal

    subroutine cleanup(this)
        class(tensor_slice), intent(inout) :: this

        if ( allocated(this%offsets) ) deallocate(this%offsets)
        if ( allocated(this%sizes) ) deallocate(this%sizes)
    end subroutine cleanup
end module tensor_slice_module
