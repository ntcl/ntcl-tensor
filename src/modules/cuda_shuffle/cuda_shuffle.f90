module cuda_shuffle_module
    use, intrinsic :: iso_fortran_env, only : &
            int64, &
            real32, &
            real64

    use :: iso_c_binding, only : &
        c_ptr, &
        c_loc, &
        c_int64_t, &
        c_float, &
        c_double, &
        c_float_complex, &
        c_double_complex, &
        c_null_ptr
    use :: cuda_data_plugin, only : cuda_synchronize_wrapper
    use :: data_api, only : stream, scratch_buffer

    use :: shuffle_module, only : shuffle 

    use :: tensor_package_api, only : &
            tensor, &
            vector, &
            dt_r32, dt_r64, dt_c64, dt_c128

    use :: tensor_construction_api, only : &
            tensor_builder

    use :: tensor_c_pointer_converter_module, only : &
            tensor_c_pointer_converter

    implicit none
    private

    public :: cuda_shuffle

    type, extends(shuffle) :: cuda_shuffle
        type(tensor_c_pointer_converter) :: converter
        type(scratch_buffer) :: pinned_scratch, device_scratch
        type(tensor_builder) :: builder
    contains
        procedure :: set_converter => set_converter
        procedure :: set_pinned_scratch => set_pinned_scratch
        procedure :: set_device_scratch => set_device_scratch
        procedure :: set_builder => set_builder
        procedure :: execute_random_copy => execute_random_copy
        procedure :: synchronize => synchronize
        procedure :: cleanup => cleanup
        procedure :: copy_index_arrays_to_device => copy_index_arrays_to_device
        procedure :: release_buffered_data => release_buffered_data
    end type cuda_shuffle

    interface cuda_shuffle
        module procedure :: constructor_empty
        module procedure :: constructor
    end interface cuda_shuffle

    interface
        subroutine cuda_execute_random_copy_real32(dst, src, didx, sidx, ndim, stream) &
                bind(C, name="cuda_execute_random_copy_real32")
            import :: c_ptr
            import :: c_int64_t
            import :: c_double

            type(c_ptr), value :: dst, src, didx, sidx
            integer(c_int64_t), value :: ndim
            type(c_ptr), value :: stream
        end subroutine cuda_execute_random_copy_real32

        subroutine cuda_execute_random_copy_real64(dst, src, didx, sidx, ndim, stream) &
                bind(C, name="cuda_execute_random_copy_real64")
            import :: c_ptr
            import :: c_int64_t
            import :: c_double

            type(c_ptr), value :: dst, src, didx, sidx
            integer(c_int64_t), value :: ndim
            type(c_ptr), value :: stream
        end subroutine cuda_execute_random_copy_real64

        subroutine cuda_execute_random_copy_complex64(dst, src, didx, sidx, ndim, stream) &
                bind(C, name="cuda_execute_random_copy_complex64")
            import :: c_ptr
            import :: c_int64_t
            import :: c_double

            type(c_ptr), value :: dst, src, didx, sidx
            integer(c_int64_t), value :: ndim
            type(c_ptr), value :: stream
        end subroutine cuda_execute_random_copy_complex64

        subroutine cuda_execute_random_copy_complex128(dst, src, didx, sidx, ndim, stream) &
                bind(C, name="cuda_execute_random_copy_complex128")
            import :: c_ptr
            import :: c_int64_t
            import :: c_double

            type(c_ptr), value :: dst, src, didx, sidx
            integer(c_int64_t), value :: ndim
            type(c_ptr), value :: stream
        end subroutine cuda_execute_random_copy_complex128
    end interface
contains            
    function constructor_empty() result(this)
        type(cuda_shuffle) :: this
    
    end function constructor_empty

    function constructor(converter, &
                         pinned_scratch, device_scratch, &
                         builder) result(this)
        type(tensor_c_pointer_converter), intent(in) :: converter
        type(scratch_buffer), intent(in) :: pinned_scratch, device_scratch 
        type(tensor_builder), intent(in) :: builder
        type(cuda_shuffle) :: this

        this = cuda_shuffle()
        call this%set_converter(converter)
        call this%set_pinned_scratch(pinned_scratch)
        call this%set_device_scratch(device_scratch)
        call this%set_builder(builder)
    end function constructor

    subroutine set_converter(this, converter)
        class(cuda_shuffle), intent(inout) :: this
        type(tensor_c_pointer_converter), intent(in) :: converter

        this%converter = converter
    end subroutine set_converter

    subroutine set_device_scratch(this, device_scratch)
        class(cuda_shuffle), intent(inout) :: this
        type(scratch_buffer), intent(in) :: device_scratch

        this%device_scratch = device_scratch
        call this%device_scratch%initialize()
    end subroutine set_device_scratch

    subroutine set_pinned_scratch(this, pinned_scratch)
        class(cuda_shuffle), intent(inout) :: this
        type(scratch_buffer), intent(in) :: pinned_scratch

        this%pinned_scratch = pinned_scratch
        call this%pinned_scratch%initialize()
    end subroutine set_pinned_scratch

    subroutine set_builder(this, builder)
        class(cuda_shuffle), intent(inout) :: this
        type(tensor_builder), intent(in) :: builder

        this%builder = builder
    end subroutine set_builder

    subroutine execute_random_copy(this, dst, src, didx, sidx, astream)
        class(cuda_shuffle), intent(inout) :: this
        class(tensor), intent(inout) :: dst
        class(tensor), intent(in) :: src
        integer(int64), dimension(:), intent(in) :: didx, sidx
        type(stream), intent(in), optional :: astream
        
        type(c_ptr) :: dst_dptr, src_dptr, actual_stream
        integer(int64) :: ndim, i
        type(vector) :: device_didx, device_sidx
        type(c_ptr) :: device_didx_cptr, device_sidx_cptr

        actual_stream = c_null_ptr
        if ( present(astream) ) actual_stream = astream%sid
        call this%converter%secure_pointer(src, src_dptr, astream)
        call this%converter%secure_pointer(dst, dst_dptr, astream)
        call this%copy_index_arrays_to_device(device_didx, device_sidx, didx, sidx, astream)
        call this%converter%secure_pointer(device_didx, device_didx_cptr, astream)
        call this%converter%secure_pointer(device_sidx, device_sidx_cptr, astream)
        ndim = size(didx, 1) 
        select case (dst%datatype)
        case (dt_r32) 
            call cuda_execute_random_copy_real32(dst_dptr, src_dptr, device_didx_cptr, &
                                            device_sidx_cptr, ndim, actual_stream)
        case (dt_r64) 
            call cuda_execute_random_copy_real64(dst_dptr, src_dptr, device_didx_cptr, &
                                            device_sidx_cptr, ndim, actual_stream)
        case (dt_c64) 
            call cuda_execute_random_copy_complex64(dst_dptr, src_dptr, device_didx_cptr, &
                                               device_sidx_cptr, ndim, actual_stream)
        case (dt_c128) 
            call cuda_execute_random_copy_complex128(dst_dptr, src_dptr, device_didx_cptr, &
                                                     device_sidx_cptr, ndim, actual_stream)
        end select
        call this%converter%update_and_release(dst, dst_dptr, astream)
        call this%converter%release(src, src_dptr, astream)
        call this%converter%release(device_didx, device_didx_cptr, astream)
        call this%converter%release(device_sidx, device_sidx_cptr, astream)
        call this%release_buffered_data(device_didx, device_sidx, this%device_scratch, astream)
    end subroutine execute_random_copy

    subroutine synchronize(this, astream)
        class(cuda_shuffle), intent(in) :: this
        type(stream), intent(in), optional :: astream

        integer :: error

        error = cuda_synchronize_wrapper(astream)

        if (error /= 0) error stop "cuda_shuffle::synchronize:Error in call to synchronize wrapper."
    end subroutine synchronize

    subroutine cleanup(this)
        class(cuda_shuffle), intent(inout) :: this

        call this%converter%cleanup()
        call this%device_scratch%cleanup()
        call this%pinned_scratch%cleanup()
        call this%builder%cleanup()
    end subroutine cleanup

    subroutine copy_index_arrays_to_device(this, device_didx, device_sidx, didx, sidx, astream)
        class(cuda_shuffle), intent(inout) :: this
        type(vector), intent(inout) :: device_didx, device_sidx
        integer(int64), dimension(:), intent(in) :: didx, sidx
        type(stream), intent(in), optional :: astream

        type(vector) :: pinned_didx, pinned_sidx

        call this%builder%copy_to_scratch(pinned_didx, this%pinned_scratch, didx)
        call this%builder%copy_to_scratch(pinned_sidx, this%pinned_scratch, sidx)

        call this%builder%copy_to_scratch(device_didx, this%device_scratch, pinned_didx, astream)
        call this%builder%copy_to_scratch(device_sidx, this%device_scratch, pinned_sidx, astream)
        
        call this%release_buffered_data(pinned_didx, pinned_sidx, this%pinned_scratch, astream)
    end subroutine copy_index_arrays_to_device

    subroutine release_buffered_data(this, pinned_didx, pinned_sidx, scratch, astream)
        class(cuda_shuffle), intent(in) :: this
        type(vector), intent(inout) :: pinned_didx, pinned_sidx
        type(scratch_buffer), intent(inout) :: scratch
        type(stream), intent(in), optional :: astream

        call scratch%destroy(pinned_didx%storage)
        call scratch%destroy(pinned_sidx%storage)
        call pinned_didx%release()
        call pinned_sidx%release()
        call scratch%checkpoint(astream)
    end subroutine release_buffered_data
end module cuda_shuffle_module

