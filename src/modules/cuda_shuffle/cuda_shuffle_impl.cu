#include <cstdint>
#include <cuComplex.h>
#include <cuda_common.h>

template <typename T>
__global__ void cuda_execute_random_copy_kernel(T *dst, T *src, uint64_t *didx, uint64_t *sidx, uint64_t ndim) {
	uint64_t idx = ((uint64_t) blockIdx.x)*((uint64_t) blockDim.x) + threadIdx.x;	

	if ( idx < ndim ) dst[didx[idx] - 1] = src[sidx[idx] - 1];
}

extern "C" void cuda_execute_random_copy_real32(float *dst, float *src, uint64_t *didx, uint64_t *sidx, uint64_t ndim,  cudaStream_t *stream) {
	int block_size = 256;
	int nblocks = (int) (ndim + 1)/block_size + 1;
	dim3 blockGrid(nblocks);
	dim3 thread_per_block(block_size);

	if ( stream ) {
		cuda_execute_random_copy_kernel<float><<<nblocks, block_size, 0, *stream>>>(dst, src, didx, sidx, ndim);	
	} else {
		cuda_execute_random_copy_kernel<float><<<nblocks, block_size>>>(dst, src, didx, sidx, ndim);	
	}
}

extern "C" void cuda_execute_random_copy_real64(double *dst, double *src, uint64_t *didx, uint64_t *sidx, uint64_t ndim,  cudaStream_t *stream) {
	int block_size = 256;
	int nblocks = (int) (ndim + 1)/block_size + 1;
	dim3 blockGrid(nblocks);
	dim3 thread_per_block(block_size);

	if ( stream ) {
		cuda_execute_random_copy_kernel<double><<<nblocks, block_size, 0, *stream>>>(dst, src, didx, sidx, ndim);	
	} else {
		cuda_execute_random_copy_kernel<double><<<nblocks, block_size>>>(dst, src, didx, sidx, ndim);	
	}
}

extern "C" void cuda_execute_random_copy_complex64(cuComplex *dst, cuComplex *src, uint64_t *didx, uint64_t *sidx, uint64_t ndim,  cudaStream_t *stream) {
	int block_size = 256;
	int nblocks = (int) (ndim + 1)/block_size + 1;
	dim3 blockGrid(nblocks);
	dim3 thread_per_block(block_size);

	if ( stream ) {
		cuda_execute_random_copy_kernel<cuComplex><<<nblocks, block_size, 0, *stream>>>(dst, src, didx, sidx, ndim);	
	} else {
		cuda_execute_random_copy_kernel<cuComplex><<<nblocks, block_size>>>(dst, src, didx, sidx, ndim);	
	}
}
extern "C" void cuda_execute_random_copy_complex128(cuDoubleComplex *dst, cuDoubleComplex *src, uint64_t *didx, uint64_t *sidx, uint64_t ndim,  cudaStream_t *stream) {
	int block_size = 256;
	int nblocks = (int) (ndim + 1)/block_size + 1;
	dim3 blockGrid(nblocks);
	dim3 thread_per_block(block_size);

	if ( stream ) {
		cuda_execute_random_copy_kernel<cuDoubleComplex><<<nblocks, block_size, 0, *stream>>>(dst, src, didx, sidx, ndim);	
	} else {
		cuda_execute_random_copy_kernel<cuDoubleComplex><<<nblocks, block_size>>>(dst, src, didx, sidx, ndim);	
	}
}
